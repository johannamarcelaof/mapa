package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LocationRecipesActivity extends AppCompatActivity {

    Button btnLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_recipes);

        btnLocation = (Button) findViewById(R.id.btnLocation);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity1.class);
                startActivity(i);

            }
        });
    }

    public void siguiente(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity2.class);
        startActivity(i);

    }
}


