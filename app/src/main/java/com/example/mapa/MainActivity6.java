package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity6 extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;

    SupportMapFragment fragment;
    private Object LatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        fragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //Barranquilla
        LatLng barranquilla = new LatLng(10.9799669, -74.8013085);
        map.addMarker(new MarkerOptions().position(barranquilla).title("Marker in Barranquilla").snippet("Limonada de coco"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(barranquilla,5));
    }

    public void atras(View view) {
        Intent i = new Intent(getApplicationContext(),LocationRecipesActivity6.class);
        startActivity(i);

    }
}