package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity8 extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;

    SupportMapFragment fragment;
    private Object LatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main8);
        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        fragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //Oaxaca
        LatLng oaxaca = new LatLng(17, -96.5);
        map.addMarker(new MarkerOptions().position(oaxaca).title("Marker in Oaxaca de Juárez").snippet("Tlayuda Oaxaqueña"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(oaxaca, 5));
    }

    public void atras(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity8.class);
        startActivity(i);

    }
}