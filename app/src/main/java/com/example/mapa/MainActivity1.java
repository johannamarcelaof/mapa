package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity1 extends AppCompatActivity implements OnMapReadyCallback {

 private GoogleMap map;

 SupportMapFragment fragment;
    private Object LatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        fragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //Bogotá
        LatLng bogota = new LatLng(4.6533326, -74.083652);
        map.addMarker(new MarkerOptions().position(bogota).title("Marker in Bogota").snippet("Ajíaco Santafereño"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(bogota,5));
    }

    public void atras(View view) {
        Intent i = new Intent(getApplicationContext(),LocationRecipesActivity.class);
        startActivity(i);

    }
    public void home(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity.class);
        startActivity(i);

    }
}
