package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity2 extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;

    SupportMapFragment fragment;
    private Object LatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        fragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //Madrid
        LatLng madrid = new LatLng(40.4167047, -3.7035825);
        map.addMarker(new MarkerOptions().position(madrid).title("Marker in Madrid").snippet("Arroz con pollo"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(madrid,5));
    }

    public void atras(View view) {
        Intent i = new Intent(getApplicationContext(),LocationRecipesActivity2.class);
        startActivity(i);

    }
    public void home(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity.class);
        startActivity(i);

    }
}

