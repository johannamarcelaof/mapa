package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LocationRecipesActivity8 extends AppCompatActivity {

    Button btnLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_recipes8);

        btnLocation = (Button) findViewById(R.id.btnLocation);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity8.class);
                startActivity(i);

            }
        });
    }

    public void anterior(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity7.class);
        startActivity(i);

    }
    public void home(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity.class);
        startActivity(i);

    }
}