package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity4 extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;

    SupportMapFragment fragment;
    private Object LatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        fragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //Tokio
        LatLng tokio = new LatLng(35.6828387, 139.7594549);
        map.addMarker(new MarkerOptions().position(tokio).title("Marker in Tokio").snippet("Chop Suey"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(tokio,5));
    }

    public void atras(View view) {
        Intent i = new Intent(getApplicationContext(),LocationRecipesActivity4.class);
        startActivity(i);

    }
}