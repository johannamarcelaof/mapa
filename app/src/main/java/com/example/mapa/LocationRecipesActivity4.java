package com.example.mapa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LocationRecipesActivity4 extends AppCompatActivity {

    Button btnLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_recipes4);

        btnLocation = (Button) findViewById(R.id.btnLocation);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity4.class);
                startActivity(i);

            }
        });
    }

    public void siguiente(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity5.class);
        startActivity(i);

    }
    public void anterior(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity3.class);
        startActivity(i);

    }
    public void home(View view) {
        Intent i = new Intent(getApplicationContext(), LocationRecipesActivity.class);
        startActivity(i);

    }
}